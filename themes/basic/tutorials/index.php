
<div id="center">
    <div id="tutorialsBlock" >

        <div class="container">
            <h1>Tutorials</h1>

            <div class="featuredProducts__search">
                <input name="search" type="search" placeholder="Search tutorials">
            </div>

            <div class="featuredProducts__item center-block">
                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_2436.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>

                <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
                    <div class="photoItemBlock">
                        <div class="featured">featured</div>
                        <h4>Creative Photoshop Techniques</h4>
                        <div class="arrow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="getDiscount">
        <div class="container">
            <div class="h2"><h2>Get 10% discount</h2></div>
            <div class="description">Subscribe to our newsletter and get discount for your first purchase</div>

            <div class="discountForm">
                <form action="" class="center-block">

                    <div class="default-form-input">
                        <input name="name" type="text" placeholder="Your Name">
                    </div>

                    <div class="default-form-input">
                        <input name="email" type="email" placeholder="Email Address">
                    </div>

                    <div class="splashfolioButton__in">
                        <button name="submit_button" class="splashfolioButton button-orange">Subscribe</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="masterclass__social">
        <div class="container">
            <h2>Stay up to date with masterclass</h2>

            <div class="masterclass__social">
                <div class="masterclass__socialItem">
                    <div class="masterclass__socialImg twitter"></div>
                </div>
                <div class="masterclass__socialItem">
                    <div class="masterclass__socialImg facebook"></div>
                </div>
                <div class="masterclass__socialItem">
                    <div class="masterclass__socialImg mail"></div>
                </div>
                <div class="masterclass__socialItem">
                    <div class="masterclass__socialImg youtube"></div>
                </div>
            </div>
        </div>
    </div>

</div>