<div id="masterclass">
    <div class="container">
        <h2>Stay up to date with masterclass</h2>

        <div class="masterclass__social">
            <?php foreach ($socials as $social) {?>
                <div class="masterclass__socialItem">
                    <a href="<?php echo $social->link;?>">
                        <div class="masterclass__socialImg"
                             style="
                                 background: url(<?php echo $social->img;?>) no-repeat center / cover;
                                 width: 33px;
                                 height: 27px;
                                 "
                        >
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
