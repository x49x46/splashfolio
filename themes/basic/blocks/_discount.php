<div id="discount">
    <div class="container">
        <div class="h2"><h2>Get 10% discount</h2></div>
        <div class="description">Subscribe to our newsletter and get discount for your first purchase</div>
        <div class="discountForm">
            <?php echo \yii\helpers\Html::beginForm('', 'post', [
                'class' => 'center-block',
                'id' => 'subscribe'
            ])?>
            <div class="form-success">
                <div class="form-success-in">
                    <div>Ваше сообщение отправленно!
                    </div>
                </div>
            </div>

            <div class="default-form-input">
                <input name="Subscribe[name]" required type="text" class="name" placeholder="Your Name">
            </div>

            <div class="default-form-input">
                <input name="Subscribe[email]" required type="email" class="email" placeholder="Email Address">
            </div>

            <div class="splashfolioButton__in">
                <button name="submit_button" class="splashfolioButton button-orange">Subscribe</button>
            </div>
            <?php echo \yii\helpers\Html::endForm();?>
        </div>
    </div>
</div>
