<?php if(!empty($banners)) {?>
    <div class="headerBlock">
        <div id="header_carousel">

            <?php foreach ($banners as $banner) {?>

                <div class="headerBlock__item">
                    <div class="headerBlock__img" style="background: url('<?php echo $banner->img;?>') no-repeat center / cover"></div>

                    <div class="headerBlock__item_in">
                        <div class="featured">featured</div>
                        <div class="h1"><?php echo $banner->name;?></div>
                        <div class="headerBlock__desc">
                            <?php echo $banner->text;?>
                        </div>
                        <div class="splashfolioButtonBlock">
                            <div class="splashfolioButton__in" style="display: inline-block;">
                                <div class="splashfolioButton button-green">Purchase tutorial</div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php }?>
        </div>

        <div class="pagin"></div>

        <div class="header_arrow">
            <div class="prev"></div>
            <div class="next"></div>
        </div>
    </div>
<?php }?>