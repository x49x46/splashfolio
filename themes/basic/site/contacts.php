<?php
    use yii\helpers\Html;
?>

<div id="center">
    <div id="contactUs">
        <div class="container">
            <h1>Contact Us</h1>

            <div id="contactUs-form" class="form">
                <?php echo Html::beginForm('', 'post', [
                    'id' => 'feedback'
                ])?>
                    <div class="form-input">
                        <label class="form-req">
                            Your email address
                        </label>
                        <input name="Feedback[email]" type="email" required>
                    </div>

                    <div class="form-input">
                        <label class="form-req">Subject</label>
                        <input name="Feedback[subject]" type="text"  required>
                    </div>

                    <div class="form-input">
                        <label class="form-req">Description</label>
                        <textarea name="Feedback[text]" required></textarea>
                    </div>

                    <div class="form-text">
                        Please enter the details of your request. A member of our support
                        staff will respond as soon as possible.
                    </div>

                    <div class="form-input">
                        <label>Attachments</label>

                        <div class="form-file">
                            <span>Add file</span> or drop files here
                        </div>
                        <input class="file hidden" name="Feedback[file]" type="file" />
                    </div>

                    <div class="add-file hidden">
                        <div class="add-file-text">photo.jpg</div>
                        <div class="add-close"></div>
                    </div>

                    <div class="splashfolioButtonBlock">
                        <div class="splashfolioButton__in">
                            <button class="splashfolioButton button-orange" name="submit_button">
                                Submit
                            </button>
                        </div>
                    </div>
                <?php echo Html::endForm();?>
            </div>
        </div>
    </div>
    <div id="thankYou" class="hidden">
        <div class="container">
            <div class="thankYou_img" style="background: url('/img/sent.png') no-repeat center / cover"></div>
            <div class="thankYou_text">Thank you!
                <span>Your request has been sent. A member of our support staff will respond as soon as possible.</span>
            </div>

            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">
                        <a href="/">
                            Go to main page
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
