<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div id="center">
    <div id="testimonials">
        <div class="container">
            <div class="h2"><h2><?php echo Yii::t('site', 'Write a review'); ?></h2></div>

            <?php $form = ActiveForm::begin(['id' => 'write-review']); ?>
            <?= $form->field($model, 'email')->textInput(['type' => 'email']); ?>
            <?= $form->field($model, 'name')->textInput(); ?>
            <?= $form->field($model, 'profession')->textInput(); ?>
            <?= $form->field($model, 'stars')->textInput(['type' => 'number', 'max' => 5]); ?>
            <?= $form->field($model, 'review_text')->textarea(['height' => 50]); ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('site', 'Send'), ['class' => 'btn button-green']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>