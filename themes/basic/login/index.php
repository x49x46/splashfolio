<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 07.07.2019
 * Time: 22:40
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div id="center">
    <div id="testimonials">
        <div class="container">
            <div class="h2"><h2><?php echo Yii::t('site', 'Login'); ?></h2></div>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

