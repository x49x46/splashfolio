<div id="top_menu">
    <div class="container">
        <div class="headerStr">
            <div class="headerStr__logo" style="background: url('/img/logo.png') no-repeat center / contain"></div>

            <div class="top_menu_block">
                <div class="top_menu_block__burger"></div>

                <div class="top_menu_block__menu">
                    <div class="headerStr__menu">
                        <ul>
                            <?php foreach ($this->context->menuHeader as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo Yii::t('site', $model->name);?>
                                </a>
                            </li>
                            <?php } ?>
                            <?php if (Yii::$app->user->isGuest): ?>
                                <li><a id="login" href="" onclick="return false;" class="button-join-community"><?php echo Yii::t('site','Login');?></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <div class="headerStr__profile">
                            <div class="headerStrLang__anchor"><?php echo $this->context->user->username; ?></div>

                            <ul>
                                <li>
                                    <a href="#"><?php echo \Yii::t('site', 'Profile');?></a>
                                </li>
                                <li>
                                    <a href="/site/exit"><?php echo \Yii::t('site', 'Logout');?></a>
                                </li>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="headerStr__lang">
                        <div class="headerStrLang__anchor">
                            <?php echo $this->context->language;?>
                        </div>

                        <?php if ($this->context->languages): ?>
                            <ul>
                                <?php foreach ($this->context->languages as $lang): ?>
                                    <li>
                                        <a href="/site/lang/<?=$lang->id?>">
                                            <?=$lang->name?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="top_menu_block__buy">
                    <?php if ($this->context->tutorial_cost): ?>
                        <div class="top_menu_block__text"><?php echo \Yii::t('site', 'All videos in one bundle. More than 70% discount.');?></div>

                        <div class="discountVideo__price">
                            <div class="splashfolioButton__in"><div class="splashfolioButton button-green"><?php echo \Yii::t('app', 'Purchase');?></div></div>
                            <div class="priceBlock">
                                <?php if ($this->context->tutorial_sale_cost): ?>
                                    <span>$<?=$this->context->tutorial_sale_cost?></span>
                                <?php endif; ?>
                                <span>$<?=$this->context->tutorial_cost?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>