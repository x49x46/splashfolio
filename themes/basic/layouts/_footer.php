<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div id="footer">
    <div class="container">
        <div class="footerBlock center-block text-align-left">
            <div class="footerBlock__left">
                <div class="footerBlock__text">
                    Splashfolio
                    <span>
                        <?php echo \Yii::t('site', 'Photography Tutorials From The Best In The Business');?>
                    </span>
                </div>

                <div class="featured"><?php echo \Yii::t('site', 'Secured with SSL');?></div>
            </div>
            <div class="footerBlock__right">
                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter1 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>

                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter2 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>

                <div class="footerBlock__menu">
                    <ul>
                        <?php foreach ($this->context->menuFooter3 as $model) {?>
                            <li>
                                <a href="<?php echo $model->link;?>">
                                    <?php echo $model->name;?>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="footerCopyright">
        <div class="container">
            <div class="footerCopyright__block center-block text-align-left">
                <div class="footerCopyright__left">© Splashfolio, <?=date('Y')?></div>
                <div class="footerCopyright__right">
                    <span>Privacy Policy</span>
                    <span>Terms of Use</span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--popup start-->
<div id="joint-popup" class="joint-popupBlock" style="display: none">
    <div class="h2"><h2><?php echo Yii::t('site', 'Login'); ?></h2></div>

    <?php $form = ActiveForm::begin(['id' => 'login-form', 'action' => '/login']); ?>

    <?= $form->field($this->context->user, 'username') ?>

    <?= $form->field($this->context->user, 'password')->passwordInput() ?>

    <?= $form->field($this->context->user, 'rememberMe')->checkbox() ?>

    <div class="splashfolioButton__in">
        <div class="form-group">
            <?= Html::submitButton('Войти', ['class' => 'splashfolioButton button-green', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<!--popup end-->