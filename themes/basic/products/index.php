<div id="header">
    <?php echo $this->render('/blocks/_banners', [
        'banners' => $banners
    ]);?>
</div>


<div id="giveaGift-popup" class="popup">

    <h2>Give a Gift</h2>

    <div class="description">Please choose an amount.</div>

    <div class="giveaGift__block">
        <div class="giveaGift__item center-block">
            <div class="giveaGift__left">50</div>
            <div class="giveaGift__center">~ 1 small tutorial</div>
            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange relatedProducts-open">Select</div>
                </div>
            </div>
        </div>

        <div class="giveaGift__item center-block">
            <div class="giveaGift__left">100</div>
            <div class="giveaGift__center">~ 2 small tutorials</div>
            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange relatedProducts-open">Select</div>
                </div>
            </div>
        </div>

        <div class="giveaGift__item center-block">
            <div class="giveaGift__left">150</div>
            <div class="giveaGift__center">~ 1 large or 3 small tutorials</div>
            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange relatedProducts-open">Select</div>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="featuredgiveaGift-popup" class="popup">

    <h2>Give a Gift</h2>

    <div class="description">What tutorial do you want to give?</div>

    <div class="featuredProducts__search">
        <input name="search" type="search" placeholder="Search tutorials">
    </div>

    <div class="featuredProducts__item center-block">
        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_9860.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_2436.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_0871_1-Pano.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3132_F.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_3399.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_4216.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_7893.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>

        <div class="photoItem" style="background: url('img/ourPhotographers/DSC_1349_Panorama2_16bit_100_67.jpg') no-repeat center / cover">
            <div class="photoItemBlock">
                <div class="featured">featured</div>
                <h4>Creative Photoshop Techniques</h4>
                <div class="arrow"></div>
            </div>
        </div>
    </div>
</div>
<div id="giveaGift-popupEmpty" class="popup">
    <h2>Give a Gift</h2>

    <div class="description">What tutorial do you want to give?</div>

    <div class="featuredProducts__search">
        <input name="search" type="search" placeholder="Search tutorials">
    </div>

    <div class="giveaGift__nothing">
        <div class="giveaGift__nothing_img" style="background: url('img/gift/empty-state.png') no-repeat center / contain"></div>
        <div class="giveaGift__nothing_text">
            Nothing Found
            <span>Please try other search phrase</span>
        </div>
    </div>
</div>



<div id="center" style="margin: 420px 0 0 0;">
    <div id="giveaGift">
        <div class="container">
            <div class="h2"><h2><?=Yii::t('site', 'Give a Gift');?></h2></div>

            <div class="description">
                <?=Yii::t('site', '
                Inspire the person who inspires you!
                Give a gift that helps them grow with
                online classes taught by the world\'s best.
                ');?>
            </div>

            <div class="giveaGift__block center-block">
                <?php foreach ($catalogs as $catalog) {?>
                <div class="giveaGift__item">
                    <div class="giveaGift__img">
                        <img src="<?php echo $catalog->img;?>">
                    </div>
                    <div class="giveaGift__title">
                        <?php echo $catalog->name;?>
                        <span>
                            <?php echo $catalog->text?>
                        </span>
                    </div>
                    <div class="giveaGift__price">
                        $<?php echo $catalog->price;?>
                    </div>

                    <div class="splashfolioButtonBlock">
                        <div class="splashfolioButton__in">
                            <div class="splashfolioButton button-orange button-featuredgiveaGift">
                                Select Tutorial
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>


    <div id="exploreTeachers">
        <div class="container">
            <div class="h2"><h2><?=Yii::t('site', 'Explore the Teachers');?></h2></div>

            <div class="exploreTeachers__block center-block">

                <?php if (!empty($teachers)): ?>
                    <?php foreach ($teachers as $teacher): ?>
                        <div class="exploreTeachers__item">
                            <div class="exploreTeachers__img" style="background: url('img/gift/face2.jpg') no-repeat center / cover">
                            </div>
                            <div class="exploreTeachers__name">
                                <?=$teacher->user->username?>
                                <span><?=$teacher->profession?></span>
                            </div>

                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <a href="/teachers">
                <div class="splashfolioButtonBlock">
                    <div class="splashfolioButton__in">
                        <div class="splashfolioButton button-orange"><?=Yii::t('site', 'See more');?></div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div id="discountVideoGift">
        <div class="container">
            <div class="discountVideoGift__block center-block">
                <div class="discountVideoGift__left display-none">
                    <div style="background: url('<?php echo $model->img;?>') no-repeat center / cover"></div>
                </div>
                <div class="discountVideoGift__right">
                    <h2><?php echo $model->name;?></h2>
                    <div class="discountVideoGift__price">
                        <div class="splashfolioButton__in">
                            <div class="splashfolioButton button-green">Purchase</div>
                        </div>
                        <div class="priceBlock">
                            <span><?php echo $model->price;?></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="discountVideoGift__desc center-block">
                        <div class="description">
                            <p><?php echo $model->text;?></p>
                        </div>
                    </div>
                </div>

                <!-- <div class="splashfolioButtonBlock discountVideoButton display-block">
                     <div class="splashfolioButton button-gray">more</div>
                 </div>-->
            </div>

            <div class="discountVideoGift__guarantee center-block">
                <div class="discountVideoGift__item">
                    <div class="h4">
                        <h4>100% Guaranteed</h4>
                    </div>
                    <div class="description">
                        We offer full refunds up to
                        30 days after purchase
                    </div>
                </div>

                <div class="discountVideoGift__item">
                    <div class="h4">
                        <h4>Learn Anywhere</h4>
                    </div>
                    <div class="description">
                        Take classes with you, and access exclusive content only available on iPhone and iPad
                    </div>
                </div>

                <div class="discountVideoGift__item">
                    <div class="h4">
                        <h4>On Demand</h4>
                    </div>
                    <div class="description">
                        Take the class at ypur own pace and in your own time
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="testimonials">
        <div class="container">
            <div class="h2"><h2>Testimonials</h2></div>

            <div class="testimonials__block center-block">
                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover">
                        </div>
                        <div class="testimonials__name">
                            Alfred Jefferson
                            <span>Photographer</span>
                        </div>
                    </div>

                    <div class="description">
                        The instructor goes through and compares lenses, explained his locations and shows its extremely valuable and sought out editing skills. The instructor goes …
                        <a href="#">more</a>
                    </div>
                </div>

                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover">
                        </div>
                        <div class="testimonials__name">
                            Nicole Knipes
                            <span>Digital Designer</span>
                        </div>
                    </div>

                    <div class="description">
                        This course is exactly what I needed to complete my photography skill set. The instructor goes through and compares lenses, explained his locations.
                    </div>
                </div>

                <div class="testimonials__item">
                    <div class="testimonials__author">
                        <div class="testimonials__avatar" style="background: url('img/ourPhotographers/user1.png') no-repeat center / cover">
                        </div>
                        <div class="testimonials__name">
                            Connor Siedow
                            <span>Creative Director</span>
                        </div>
                    </div>

                    <div class="description">
                        Seriously worth every penny and is worth way more than $ could by! I found it essential for the next steps on my photography career.
                    </div>
                </div>
            </div>

            <div class="testimonialsButton">
                <div class="splashfolioButton__in button-gray">
                    <div class="splashfolioButton button-join-community">Add review</div>
                </div>
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-orange">See more</div>
                </div>
            </div>
        </div>
    </div>


    <?php echo $this->render('/blocks/_discount')?>


    <div id="joint-popup" class="popup">

        <h2>Join the Creative Community</h2>

        <div class="description">Log in or sign up for free access to on air classes led by the world's top experts. The creative community is stronger with you in it!</div>

        <div class="joint-popupBlock">
            <div class="splashfolioButtonBlock">
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-blue">Continue With Facebook</div>
                </div>
                <div class="splashfolioButton__in">
                    <div class="splashfolioButton button-red">Continue With Google</div>
                </div>
            </div>

            <div class="joint-popupBlock_text">
                <div class="description">We will never post without asking</div>
                <div class="description">Or Continue With Email</div>
            </div>

            <div class="jointForm discountForm">
                <form action="" class="center-block">
                    <div class="default-form-input">
                        <input name="email" type="email" placeholder="Email Address">
                    </div>

                    <div class="splashfolioButton__in">
                        <button name="submit_button" class="splashfolioButton button-orange">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
