<?php
use yii\helpers\Html;

$this->title = Yii::t('admin', 'Create translate');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('admin', 'List of translates'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="translate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
