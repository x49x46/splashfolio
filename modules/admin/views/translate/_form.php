<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    //'action' => ['create'],
    'method' => 'post',
]);

?>

<?= $form->field($model, 'category') ?>

<?= $form->field($model, 'message') ?>

<?= $form->field($model, 'language') ?>

<?= $form->field($model, 'translation') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>