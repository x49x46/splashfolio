<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]); ?>

    <?php if($model->img) {
        echo "<img src=\"$model->img\" width=\"200px\" />";
    }

    ?>
    <?= $form->field($model, 'img')->fileInput() ?>


    <?= $form->field($model, 'lang_id')->widget(Select2::classname(), [
        'data' => \app\models\Lang::getLangsArray(),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите язык'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'is_active')->checkbox(); ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
