<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SoftwareCatalog */

$this->title = Yii::t('admin', 'Create Software Catalog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Software Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="software-catalog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
