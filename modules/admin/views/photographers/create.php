<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 05.06.2019
 * Time: 20:56
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lang */

$this->title = Yii::t('admin', 'Add photographer');
$this->params['breadcrumbs'][] = ['label' => 'Langs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photographer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model_photographer' => $model_photographer,
        'model_image' => $model_image
    ]) ?>

</div>