<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 07.06.2019
 * Time: 09:07
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\promo\OurPhotographers */

$this->title = Yii::t('admin', 'Options');
$this->params['breadcrumbs'][] = ['label' => 'our photographers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="photographer-misc">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'block_count')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'button_caption')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
