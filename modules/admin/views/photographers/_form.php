<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 05.06.2019
 * Time: 20:54
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$url = \yii\helpers\Url::to(['users/search']);
?>

<div class="photographer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model_photographer, 'user_id')->widget(Select2::classname(), [
        'initValueText' => '',
        'model' => $model_photographer,
        'attribute' => 'username',
        'options' => ['placeholder' => 'Выберите пользователя'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.username; }'),
            'templateSelection' => new JsExpression('function (user) { return user.username; }'),
        ],
    ]); ?>

    <?= $form->field($model_image, 'img')->fileInput() ?>

    <?= $form->field($model_photographer, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model_photographer, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]); ?>

    <?= $form->field($model_photographer, 'dark_text')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
