<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MenuHeaderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Headers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-header-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Menu Header', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'link',
            [
                'attribute' => 'is_active',
                'label' => 'Активность',
                'value' => function($model) {
                    if($model->is_active == 1) {
                        return 'Активен';
                    } else {
                        return 'Не автивен';
                    }
                },
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'MenuHeaderSearch[is_active]',
                    'data' => [0 => 'Не активен', 1 => 'Активен'],
                    'value' => $searchModel->is_active,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите значение'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            'sort',
            [
                'attribute' => 'lang_id',
                'label' => 'Язык',
                'value' => function($model) {
                    return $model->lang->name;
                },
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'MenuHeaderSearch[lang_id]',
                    'data' => \app\models\Lang::getLangsArray(),
                    'value' => $searchModel->lang_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите значение'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
