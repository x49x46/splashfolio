<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuHeader */

$this->title = 'Create Menu Header';
$this->params['breadcrumbs'][] = ['label' => 'Menu Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
