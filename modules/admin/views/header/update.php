<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuHeader */

$this->title = 'Update Menu Header: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Menu Headers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-header-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
