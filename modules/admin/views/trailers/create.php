<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trailer */

$this->title = Yii::t('admin', 'Create Trailer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Trailers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trailer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
