<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MenuFooter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-footer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'column')->widget(Select2::classname(), [
        'data' => \app\models\MenuFooter::$COLUMN_TYPE,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите язык'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'sort')->textInput() ?>


    <?= $form->field($model, 'lang_id')->widget(Select2::classname(), [
        'data' => \app\models\Lang::getLangsArray(),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите язык'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
