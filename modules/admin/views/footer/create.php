<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuFooter */

$this->title = 'Create Menu Footer';
$this->params['breadcrumbs'][] = ['label' => 'Menu Footers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-footer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
