<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MenuFooterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu Footers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-footer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Menu Footer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'column',
            'name',
            'link',
            [
                'attribute' => 'is_active',
                'label' => 'Активность',
                'value' => function($model) {
                    if($model->is_active == 1) {
                        return 'Активен';
                    } else {
                        return 'Не автивен';
                    }
                },
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'MenuFooterSearch[is_active]',
                    'data' => [0 => 'Не активен', 1 => 'Активен'],
                    'value' => $searchModel->is_active,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите значение'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute' => 'lang_id',
                'label' => 'Язык',
                'value' => function($model) {
                    return $model->lang->name;
                },
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'MenuFooterSearch[lang_id]',
                    'data' => \app\models\Lang::getLangsArray(),
                    'value' => $searchModel->lang_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите значение'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
