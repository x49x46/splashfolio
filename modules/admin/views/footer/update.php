<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuFooter */

$this->title = 'Update Menu Footer: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Menu Footers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-footer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
