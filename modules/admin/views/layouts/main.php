<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use app\assets\AdminAsset;
use yii\helpers\Html;

$bundle = yiister\gentelella\assets\Asset::register($this);
AdminAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>">
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-paw"></i>
                        <span>
                            <? echo Yii::t('admin', 'Control panel') ?>
                        </span>
                    </a>
                </div>
                <div class="clearfix"></div>

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => [
                                    [
                                        'label' => Yii::t('admin', 'Images'),
                                        'url' => '/admin/images'
                                    ],
                                    [
                                        'label' => Yii::t('admin', 'Videos'),
                                        'url' => '/admin/video'
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Верхнее меню'),
                                        "url" => "/admin/header/index"
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Нижнее меню'),
                                        "url" => "/admin/footer/index"
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Вопрос ответ'),
                                        "url" => "/admin/faq/index"
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Обратная связь'),
                                        "url" => "/admin/feedback/index"
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Каталог'),
                                        "url" => "/admin/catalogs/index"
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Software'),
                                        "url" => "/admin/software"
                                    ],
                                    [
                                        "label" => Yii::t('admin', 'Main page'), "url" => "#",
                                        'items' => [
                                            [
                                                "label" => Yii::t("admin", 'Banners'),
                                                "url" => "/admin/banners",
                                            ],
                                            [
                                                "label" => Yii::t("admin", 'Our photographers'),
                                                'url' => '#',
                                                'items' => [
                                                        [
                                                            "label" => Yii::t("admin", 'List'),
                                                            'url' => '/admin/photographers',
                                                        ],
                                                        [
                                                            "label" => Yii::t("admin", 'Create'),
                                                            'url' => '/admin/photographers/create',
                                                        ],
                                                        [
                                                            "label" => Yii::t("admin", 'Options'),
                                                            'url' => '/admin/photographers/options',
                                                        ]
                                                ]
                                            ],
                                            [
                                                'label' => Yii::t('admin', 'Reviews'),
                                                'url' => '/admin/reviews'
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Подписка'),
                                                'url' => '/admin/subscribes',
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Иконки'),
                                                'url' => '/admin/socials',
                                            ]
                                        ]
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Languages'), "url" => "#",
                                        "items" => [
                                            [
                                                "label" => Yii::t("admin", 'Languages'),
                                                "url" => "/admin/langs",
                                            ],
                                            [
                                                "label" => Yii::t("admin", 'Translate'),
                                                "url" => "/admin/translate",
                                            ],
                                            [
                                                "label" => Yii::t("admin", 'Add translate'),
                                                "url" => "/admin/translate/create",
                                            ]
                                        ]
                                    ],
                                    [
                                        "label" => Yii::t("admin", 'Users'),
                                        "url" => "/admin/users/index"
                                    ],
                                    [
                                        'label' => Yii::t('admin', 'Teachers'),
                                        'url' => '/admin/teachers'
                                    ],
                                    [
                                        'label' => Yii::t('admin', 'Tutorials'),
                                        'url' => '/admin/tutorials'
                                    ],
                                    [
                                        'label' => Yii::t('admin', 'Trailers'),
                                        'url' => '/admin/trailers'
                                    ],
                                    [
                                        "label" => Yii::t('admin', 'Exit'),
                                        "url" => "/admin/site/logout"
                                    ],
                                ],
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right"></div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
