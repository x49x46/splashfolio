<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Tutorial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tutorial-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_id')->textInput() ?>

    <?php echo $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]); ?>

    <?=\yii\bootstrap\Collapse::widget([
        'items' => [
            Yii::t('admin', 'What\'s inside') => [
                'content' =>
                '<div id="inside">' .
                $form->field($model_block_inside, 'title[]')->textInput() .
                $form->field($model_block_inside, 'content[0]')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset' => 'basic'
                ]) .
                '</div>' .
                Html::a('Добавить', null, [
                    'id' => 'add-inside-block',
                    'class' => 'btn btn-success',
                ])
            ]
        ]
    ]);?>

    <hr>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="tpl-inside-block" class="">
    <?=$form->field($model_block_inside, 'title[]')->textInput()?>
    <?=$form->field($model_block_inside, 'content[1]')->widget(CKEditor::className(), [
        'options' => ['rows' => 3],
        'preset' => 'basic'
    ])?>
</div>

<script>
    $(document).ready(function() {
        $('#add-inside-block').on('click', function() {
            $('#inside').append($('#tpl-inside-block').html());
        });
    });
</script>
