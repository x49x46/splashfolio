<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tutorial */

$this->title = Yii::t('admin', 'Create Tutorial');
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Tutorials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tutorial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_block_inside' => $model_block_inside
    ]) ?>

</div>
