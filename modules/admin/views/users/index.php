<?php
/**
 * Грид пользователей
 * @var $this \yii\web\View
 * @var $dataProvider object DataProvider
 * @var $searchModel object User
 */

use yiister\gentelella\widgets\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\bootstrap\Alert;

$this->title = Yii::t('app', 'Справочник номеров гостиниц');


echo Html::a(Button::widget([
    'label' => 'Создать',
    'options' => ['class' => 'btn-primary'],
]),  ['users/create']);


if(Yii::$app->session->getFlash('error')) {
    Alert::begin([
        'options' => [
            'class' => 'alert-error',
        ],
    ]);
    echo Yii::$app->session->getFlash('error');
    Alert::end();
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'username',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}'
        ]
    ]
]);

