<?php


namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

class TranslateMessage extends Model
{
    public $id;
    public $category;
    public $language;
    public $message;
    public $translation;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['message', 'language', 'translation', 'category'], 'string'],
            [['message', 'language', 'translation', 'category'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'translation' => Yii::t('admin', 'Translation'),
            'language' => Yii::t('admin', 'Language'),
            'message' => Yii::t('admin', 'Message'),
            'category' => Yii::t('admin', 'Category')
        ];
    }
}