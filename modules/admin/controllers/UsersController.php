<?php

namespace app\modules\admin\controllers;


use Yii;
use app\modules\admin\components\AdminController;
use app\models\User;
use yii\db\Query;


/**
 * Class NumbersController
 * @package app\modules\admin\controllers
 */
class UsersController extends AdminController
{

    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new User();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['users/index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = User::getModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect(['users/index']);
        }
        return $this->render('update', ['model' => $model]);
    }


    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = User::getModel($id);
        $model->delete();
        $this->redirect(['users/index']);
    }

    public function actionSearch($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'username' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select('id, username')
                ->from('user')
                ->where(['like', 'username', $q])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'username' => User::find($id)->all()->username];
        }
        return $out;
    }
}
