<?php

namespace app\modules\admin\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\BaseController;
use app\models\User;


/**
 * Class NumbersController
 * @package app\modules\admin\controllers
 */
class SiteController extends BaseController
{


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['users/index']);
        }

        $model = new User(['scenario' => 'login']);
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['users/index']);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


}
