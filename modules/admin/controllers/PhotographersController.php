<?php

namespace app\modules\admin\controllers;

use app\models\Image;
use Yii;
use app\models\Options;
use app\models\promo\OurPhotographers;
use app\models\search\OurPhotographersSearch;
use app\modules\admin\models\PhotograperOptions;
use app\modules\admin\components\AdminController;

class PhotographersController extends AdminController
{
    public function actionIndex()
    {
        $searchModel = new OurPhotographersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model_image = new Image();
        $model_photographer = new OurPhotographers();

        if ($model_image->load(Yii::$app->request->post()) && $model_image->save()) {
            $model_photographer->image_id = $model_image->id;
            if ($model_photographer->load(Yii::$app->request->post()) && $model_photographer->save()) {
                return $this->redirect(['view', 'id' => $model_photographer->id]);
            }
        }

        return $this->render('create', [
            'model_photographer' => $model_photographer,
            'model_image' => $model_image
        ]);
    }

    public function actionView($id)
    {
        $model = OurPhotographers::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        $model_photographer = OurPhotographers::findOne($id);
        $model_image = Image::findOne($model_photographer->image_id);

        if ($model_image && $model_image->load(Yii::$app->request->post()) && $model_image->save()) {
            $model_photographer->image_id = $model_image->id;
            if ($model_photographer->load(Yii::$app->request->post()) && $model_photographer->save()) {
                return $this->redirect(['view', 'id' => $model_photographer->id]);
            }
        }

        return $this->render('update', [
            'model_photographer' => $model_photographer,
            'model_image' => $model_image
        ]);
    }

    /**
     * Редактирование заголовка и описания блока "Our photographers"
     *
     * @return string
     */
    public function actionOptions()
    {
        $option_title = Options::findByKey('promo_our_photographers_title');
        $option_description = Options::findByKey('promo_our_photographers_description');
        $option_count = Options::findByKey('promo_our_photographers_count');
        $option_button = Options::findByKey('promo_our_photographers_button');

        $model = new PhotograperOptions();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            if ($option_count) {
                $option_count->value = $model->block_count;
                $option_count->save();
            }

            if ($option_title) {
                $option_title->value = $model->title;
                $option_title->save();
            }

            if ($option_description) {
                $option_description->value = $model->description;
                $option_description->save();
            }

            if ($option_button) {
                $option_button->value = $model->button_caption;
                $option_button->save();
            }
        }

        $model->block_count = $option_count ? $option_count->value : null;
        $model->title = $option_title ? $option_title->value : $option_title;
        $model->description = $option_description ? $option_description->value : null;
        $model->button_caption = $option_button ? $option_button->value : null;

        return $this->render('options', [
            'model' => $model,
        ]);
    }
}
