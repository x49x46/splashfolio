<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 02.06.2019
 * Time: 21:16
 */

namespace app\modules\admin\controllers;

use app\models\search\TranslateSearch;
use app\modules\admin\components\AdminController;
use app\models\Message;
use app\models\SourceMessage;
use app\modules\admin\models\TranslateMessage;
use Yii;

class TranslateController extends AdminController
{
    public function actionIndex()
    {
        $searchModel = new TranslateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new TranslateMessage();

        if ($model->load(Yii::$app->request->post())) {
            if (!$source_message = SourceMessage::findOne(['message' => $model->message])) {
                $source_message = new SourceMessage();
                $source_message->category = $model->category;
                $source_message->message = $model->message;
                $source_message->save();
            }

            $message = new Message();
            $message->id = $source_message->id;
            $message->language = $model->language;
            $message->translation = $model->translation;

            if ($message->save()) {
                return $this->redirect([
                    'view',
                    'id' => $message->id,
                    'language' => $model->language
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id, $language)
    {
        if (!$message = Message::findOne(['id' => $id, 'language' => $language])) {

        }

        if (!$source_message = SourceMessage::findOne($id)) {

        }

        $model = new TranslateMessage();

        if ($model->load(Yii::$app->request->post())) {
            $source_message->category = $model->category;
            $source_message->message = $model->message;

            $message->language = $model->language;
            $message->translation = $model->translation;

            if ($source_message->save() and $message->save()) {
                return $this->redirect([
                    'view',
                    'id' => $id,
                    'language' => $model->language
                ]);
            }
        }

        $model->id = $source_message->id;
        $model->category = $source_message->category;
        $model->message = $source_message->message;
        $model->language = $message->language;
        $model->translation = $message->translation;

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDelete($id, $language)
    {
        if (!$message = Message::findOne(['id' => $id, 'language' => $language])) {

        }

        if (!$source_message = SourceMessage::findOne($id)) {

        }

        $count = count($source_message->messages);

        return $this->redirect('index');
    }

    public function actionView($id, $language)
    {
        $model = new TranslateMessage();
        if (!$source_message = SourceMessage::findOne($id)) {

        }

        if (!$message = Message::findOne(['id' => $id, 'language' => $language])) {

        }

        $model->id = $source_message->id;
        $model->category = $source_message->category;
        $model->message = $source_message->message;
        $model->language = $message->language;
        $model->translation = $message->translation;

        return $this->render('view', [
            'model' => $model
        ]);
    }
}