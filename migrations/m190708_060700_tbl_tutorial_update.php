<?php

use yii\db\Migration;

/**
 * Class m190708_060700_tbl_tutorial_update
 */
class m190708_060700_tbl_tutorial_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tutorial', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tutorial', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190708_060700_tbl_tutorial_update cannot be reverted.\n";

        return false;
    }
    */
}
