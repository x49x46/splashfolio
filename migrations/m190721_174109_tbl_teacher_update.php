<?php

use yii\db\Migration;

/**
 * Class m190721_174109_tbl_teacher_update
 */
class m190721_174109_tbl_teacher_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%teacher}}', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%teacher}}', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190721_174109_tbl_teacher_update cannot be reverted.\n";

        return false;
    }
    */
}
