<?php

use yii\db\Migration;

/**
 * Class m190705_005705_tbl_trailer
 */
class m190705_005705_tbl_trailer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%trailer}}', [
            'id' => $this->primaryKey(),
            'tutorial_id' => $this->integer()->notNull(),
            'url' => $this->string(),
            'file' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%trailer}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190705_005705_tbl_trailer cannot be reverted.\n";

        return false;
    }
    */
}
