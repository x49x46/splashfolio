<?php

use yii\db\Migration;

/**
 * Class m190624_165341_tbl_tutorials
 */
class m190624_165341_tbl_tutorial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tutorial', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'image_id' => $this->integer(),
            'content' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tutorial');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190624_165341_tbl_tutorials cannot be reverted.\n";

        return false;
    }
    */
}
