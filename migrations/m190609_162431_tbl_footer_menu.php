<?php

use yii\db\Migration;

/**
 * Class m190609_162431_tbl_footer_menu
 */
class m190609_162431_tbl_footer_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu_footer}}', [
            'id' => $this->primaryKey(),
            'column' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'link' => $this->string()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(500),
            'lang_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('menu_footer_lang',
            '{{%menu_footer}}', 'lang_id',
            '{{%lang}}', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu_footer}}');
    }
}
