<?php

use yii\db\Migration;

/**
 * Class m190607_041511_promo_our_photographers_misc
 */
class m190607_041511_options extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('options', [
            'key' => $this->string(50)->notNull()->unique(),
            'value' => $this->text()
        ],'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createIndex('idx-key', 'options', 'key');

        $this->insert('options', [
            'key' =>  'promo_our_photographers_title',
            'value' => 'Our Photographers'
        ]);

        $this->insert('options', [
            'key' =>  'promo_our_photographers_description',
            'value' => 'For composite photographers, creating images is bound only by imagination. 
            Dream like photographs with incredible realism can be built from a series of different, 
            and often unrelated, components.'
        ]);

        $this->insert('options', [
            'key' =>  'promo_our_photographers_button',
            'value' => 'All tutorials'
        ]);

        $this->insert('options', [
            'key' =>  'promo_our_photographers_count',
            'value' => '2'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('options');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190607_041511_promo_our_photographers_misc cannot be reverted.\n";

        return false;
    }
    */
}
