<?php

use yii\db\Migration;

/**
 * Class m190612_083102_reviews
 */
class m190612_083102_tbl_review extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('review', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'profession' => $this->string()->notNull(),
            'review_text' => $this->text()->notNull(),
            'stars' => $this->integer()->defaultValue(0),
            'position' => $this->integer()->defaultValue(0),
            'is_published' =>  $this->tinyInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('review');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190612_083102_reviews cannot be reverted.\n";

        return false;
    }
    */
}
