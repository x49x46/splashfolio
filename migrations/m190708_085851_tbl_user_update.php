<?php

use yii\db\Migration;

/**
 * Class m190708_085851_tbl_user_update
 */
class m190708_085851_tbl_user_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'avatar', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'avatar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190708_085851_tbl_user_update cannot be reverted.\n";

        return false;
    }
    */
}
