<?php

use yii\db\Migration;

/**
 * Class m190617_105106_tbl_catalog
 */
class m190617_105106_tbl_catalog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'price' => $this->integer()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(0),
            'lang_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%catalog}}');
    }
}
