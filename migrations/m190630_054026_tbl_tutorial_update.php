<?php

use yii\db\Migration;

/**
 * Class m190630_054026_tbl_tutorial_update
 */
class m190630_054026_tbl_tutorial_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tutorial', 'cost', $this->integer()->notNull());
        $this->addColumn('tutorial', 'sale_cost', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tutorial', 'cost');
        $this->dropColumn('tutorial', 'sale_cost');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190630_054026_tbl_tutorial_update cannot be reverted.\n";

        return false;
    }
    */
}
