<?php

use yii\db\Migration;

/**
 * Class m190607_074757_tbl_works
 */
class m190607_074757_tbl_works extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu_header}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'link' => $this->string()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(500),
            'lang_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('menu_header_lang',
            '{{%menu_header}}', 'lang_id',
            '{{%lang}}', 'id'
        );

        $this->addColumn('{{%banner}}', 'is_active', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('{{%banner}}', 'sort', $this->integer()->notNull()->defaultValue(500));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu_header}}');

        $this->dropColumn('{{%banner}}', 'is_active');
        $this->dropColumn('{{%banner}}', 'sort');
    }
}
