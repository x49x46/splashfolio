<?php

use yii\db\Migration;

/**
 * Class m190725_151524_tbl_tutorial_inside_block
 */
class m190725_151524_tbl_tutorial_inside_block extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tutorial_inside_block}}', [
            'id' => $this->primaryKey(),
            'tutorial_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tutorial_inside_block}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190725_151524_tbl_tutorial_inside_block cannot be reverted.\n";

        return false;
    }
    */
}
