<?php

use yii\db\Migration;

/**
 * Class m190623_063432_tbl_review_update
 */
class m190623_063432_tbl_review_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('review', 'email', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('review', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190623_063432_tbl_review_update cannot be reverted.\n";

        return false;
    }
    */
}
