<?php

use yii\db\Migration;

/**
 * Class m190704_171029_tbl_software_update
 */
class m190704_171029_tbl_software_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('software', 'tutorial_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('software', 'tutorial_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190704_171029_tbl_software_update cannot be reverted.\n";

        return false;
    }
    */
}
