<?php

use yii\db\Migration;

/**
 * Class m190721_175336_tbl_equipment
 */
class m190721_175336_tbl_equipment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%equipment}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'equipment_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('{{%equipment_catalog}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'brand' => $this->string()->notNull(),
            'model' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('equipment_catalog',
            '{{%equipment}}', 'equipment_id',
            '{{%equipment_catalog}}', 'id'
        );

        $this->addForeignKey('user_equipment',
            '{{%equipment}}', 'user_id',
            '{{%user}}', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190721_175336_tbl_equipment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190721_175336_tbl_equipment cannot be reverted.\n";

        return false;
    }
    */
}
