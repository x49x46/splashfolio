<?php

use yii\db\Migration;

/**
 * Class m190626_161110_tbl_tutorial_update
 */
class m190626_161110_tbl_tutorial_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tutorial', 'teacher_id', $this->integer()->notNull());

        $this->addForeignKey('teacher_tutorial',
            '{{%tutorial}}', 'teacher_id',
            '{{%teacher}}', 'user_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tutorial', 'teacher_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190626_161110_tbl_tutorial_update cannot be reverted.\n";

        return false;
    }
    */
}
