<?php

use yii\db\Migration;

/**
 * Class m190609_055102_tbl_promo_our_photographers_add_dark_text
 */
class m190609_055102_tbl_promo_our_photographers_add_dark_text extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('promo_our_photographers', 'dark_text', $this->tinyInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('promo_our_photographers', 'dark_text');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190609_055102_tbl_promo_our_photographers_add_dark_text cannot be reverted.\n";

        return false;
    }
    */
}
