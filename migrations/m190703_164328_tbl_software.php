<?php

use yii\db\Migration;

/**
 * Class m190703_164328_tbl_software
 */
class m190703_164328_tbl_software extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%software}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'soft_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createTable('{{%software_catalog}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'version' => $this->string(),
            'redaction' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('soft_catalog',
            '{{%software}}', 'soft_id',
            '{{%software_catalog}}', 'id'
        );

        $this->addForeignKey('user_soft',
            '{{%software}}', 'user_id',
            '{{%user}}', 'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%software}}');
        $this->dropTable('{{%software_catalog}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_164328_tbl_software cannot be reverted.\n";

        return false;
    }
    */
}
