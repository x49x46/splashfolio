<?php

use yii\db\Migration;

/**
 * Class m190608_113644_teacher
 */
class m190608_113644_tbl_teacher extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('teacher', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey('user_teacher',
            '{{%teacher}}', 'user_id',
            '{{%user}}', 'id'
        );

        $this->createIndex('teacher', 'teacher', ['user_id']);
        $this->createIndex('teacher_lang', 'teacher', ['user_id', 'lang_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('teacher');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190608_113644_teacher cannot be reverted.\n";

        return false;
    }
    */
}
