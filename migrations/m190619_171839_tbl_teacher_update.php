<?php

use yii\db\Migration;

/**
 * Class m190619_171839_tbl_teacher_update
 */
class m190619_171839_tbl_teacher_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('teacher', 'profession', $this->string()->notNull());
        $this->addColumn('teacher', 'is_active', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('teacher', 'profession');
        $this->dropColumn('teacher', 'is_active');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190619_171839_tbl_teacher_update cannot be reverted.\n";

        return false;
    }
    */
}
