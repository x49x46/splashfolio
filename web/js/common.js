$(document).ready(function () {

    $('#subscribe').submit(function () {
        var link = $(this);
        $.ajax({
            type: "POST",
            url: '/subscribes/create',
            dataType: 'json',
            data: link.serialize(),
            success: function(data){
                if(data.success) {
                    $('#subscribe .form-success').css('display', 'block');
                } else {
                    alert('Вы не правильно заполнили форму');
                }
            },
            error: function () {
                alert('Пошло что то не так попробуйте позже');
            }
        });
        return false;
    });


    $('#feedback .form-file span').click(function () {
        $('#feedback .file').click();
        return false;
    });


    var options = {
        url:    '/feedback/create',
        data: $('#feedback').serialize(),
        type: 'post',
        dataType: 'json',
        success: function(data) {
            if(data.success) {
                $('#thankYou').removeClass('hidden');
                $('#feedback').addClass('hidden');
            }
        },
        error: function () {
            alert('Пошло что то не так попробуйте позже');
        }
    };

    $('#feedback').ajaxForm(options);
});