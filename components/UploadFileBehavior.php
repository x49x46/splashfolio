<?php

namespace app\components;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * Class UploadFileBehavior
 * @package app\components
 */
class UploadFileBehavior extends Behavior
{

    /**
     * @var array $attributes
     */
    public $attributes;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Функция инициализации настроек
     */
    public function init()
    {
        ini_set("max_execution_time", 3600);
    }

    /**
     * Подключение событий
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => ActiveRecord::EVENT_BEFORE_VALIDATE,
            ActiveRecord::EVENT_BEFORE_INSERT => ActiveRecord::EVENT_BEFORE_INSERT,
            ActiveRecord::EVENT_BEFORE_UPDATE => ActiveRecord::EVENT_BEFORE_UPDATE,
            ActiveRecord::EVENT_BEFORE_DELETE => ActiveRecord::EVENT_BEFORE_DELETE
        ];
    }

    /**
     * Событие до валидации
     * @return bool
     */
    public function beforeValidate($event)
    {
        foreach($this->attributes as $attribute) {
            $this->owner->{$attribute} = UploadedFile::getInstance($this->owner, $attribute);
        }
        return true;
    }

    /**
     * Событие до вставки модели
     * @param $event
     * @return bool
     */
    public function beforeInsert($event)
    {
        foreach($this->attributes as $attribute) {
            if($this->owner->{$attribute} instanceof UploadedFile) {
                $this->saveFileOnAttribute($attribute);
            }
        }
        return true;
    }

    /**
     * Событие обновления
     * @param $event
     * @return bool
     */
    public function beforeUpdate($event)
    {
        $className = $this->owner->className();
        $model = $className::getModel($this->owner->id);
        foreach($this->attributes as $attribute) {
            if($this->owner->{$attribute} && $model->{$attribute} !== $this->owner->{$attribute}) {
                if($model->{$attribute}) {
                    FileHelper::unlink(Yii::getAlias('@app/web').$model->{$attribute});
                }
                $this->saveFileOnAttribute($attribute);
            } else {
                $this->owner->{$attribute} = $model->{$attribute};
            }
        }
        return true;
    }

    /**
     * Событие до удаления модели
     * @param $event
     * @return bool
     */
    public function beforeDelete()
    {
        foreach($this->attributes as $attribute) {
            if($this->owner->{$attribute}) {
                FileHelper::unlink(Yii::getAlias('@app/web').$this->owner->{$attribute});
            }
        }
        return true;
    }

    /**
     * Сохраняет файлы и присваивает путь аттрибутам модели
     * @param string $attribute
     */
    public function saveFileOnAttribute($attribute)
    {
        $className = $this->owner->className();
        $path = $className::PATH_URL.$className::getNameTable();
        $file = $path.'/'.uniqid().'.'.$this->owner->{$attribute}->extension;
        if(!file_exists(Yii::getAlias('@app/web').$path)) {
            mkdir(Yii::getAlias('@app/web').$path, '0777', true);
            chmod(Yii::getAlias('@app/web').$path, 0777);
        }
        $this->owner->{$attribute}->saveAs(Yii::getAlias('@app/web').$file);
        $this->owner->{$attribute} = $file;
    }
}
