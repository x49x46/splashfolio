<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 01.06.2019
 * Time: 15:38
 */

namespace app\utils;

use Yii;
use app\models\Lang;

class LanguageUtil
{
    const DEFAULT_LANGUAGE = 'EN';

    public function setId($id)
    {
        Yii::$app->session->set('lang_id', $id);

        return $this;
    }

    public function getId()
    {
        return Yii::$app->session->get('lang_id');
    }

    public function getLang()
    {
        if (!$id = Yii::$app->session->get('lang_id')) {

        }

        return Lang::findOne($id);
    }

    public function getCode()
    {
        if (!$lang = $this->getLang()) {
            return self::DEFAULT_LANGUAGE;
        }

        return $lang->code;
    }

    public function getName()
    {
        if (!$lang = $this->getLang()) {
            return false;
        }

        return $lang->name;
    }
}