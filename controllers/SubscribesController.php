<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use app\models\Subscribe;

/**
 * Class SubscribesController
 * @package app\controllers
 */
class SubscribesController extends Controller
{


    /**
     * @return object
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        if(!Yii::$app->request->isAjax) {
            throw new HttpException(400, 'Bad request');
        }

        $success = false;
        $model = new Subscribe();
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
        }

        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => [
                'success' => $success,
                'errors' => $model->errors
            ],
        ]);
    }




}
