<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Banner;
use app\models\Feedback;
use app\models\MenuHeader;
use app\models\Social;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Lang;
use app\utils\LanguageUtil;

class FeedbackController extends BaseController
{

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Feedback();
        $success = false;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
        }

        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => [
                'errors' => $model->errors,
                'success' => $success,
            ],
        ]);
    }

}
