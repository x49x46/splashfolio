<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 07.07.2019
 * Time: 22:58
 */

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Social;
use app\components\BaseController;

class LoginController extends BaseController
{
    public function actionIndex()
    {
        $model = new User(['scenario' => 'login']);
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/']);
        } else {
            $socials = Social::find()
                ->where(['is_active' => 1])
                ->orderBy('sort')
                ->all();

            return $this->render('index', [
                'model' => $model,
                'socials' => $socials
            ]);
        }
    }

}