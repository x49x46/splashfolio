<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Tutorial;
use app\models\Video;
use app\models\Software;

/**
 * Class TutorialsController
 * @package app\controllers
 */
class TutorialsController extends BaseController
{


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionView($id)
    {
        if (!$tutorial = Tutorial::findOne($id)) {
            $this->redirect('/notfound');
        }

        $this->tutorial_cost  = $tutorial->cost;
        $this->tutorial_sale_cost  = $tutorial->sale_cost;

        $videos_count = Video::find(['tutorial_id' => $id])->count();
        $software = Software::findAll(['tutorial_id' => $id]);

        return $this->render('view', [
            'tutorial' => $tutorial,
            'software' => $software,
            'videos_count' => $videos_count
        ]);
    }
}
