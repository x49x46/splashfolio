<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Faq;
use app\models\Social;

/**
 * Class FaqController
 * @package app\controllers
 */
class FaqController extends BaseController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $faqs = Faq::find()->all();
        $socials = Social::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        return $this->render('index', [
            'faqs' => $faqs,
            'socials' => $socials
        ]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $faq = Faq::getModel($id);
        $faqs = Faq::find()->all();
        $socials = Social::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        return $this->render('view', [
            'faq' => $faq,
            'faqs' => $faqs,
            'socials' => $socials
        ]);
    }
}
