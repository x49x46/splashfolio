<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\Banner;
use app\models\Catalog;
use app\models\MenuHeader;
use app\models\Social;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Lang;
use app\models\Teacher;
use yii\db\Expression;
use app\utils\LanguageUtil;

/**
 * Class ProductsController
 * @package app\controllers
 */
class ProductsController extends BaseController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $teachers = Teacher::find()
            ->where([
                'is_active' => 1,
                'lang_id' => (new LanguageUtil())->getId()
            ])
            ->orderBy(new Expression('rand()'))
            ->all();

        $banners = Banner::find()
            ->where(['is_active' => 1])
            ->orderBy('sort')
            ->all();

        $catalogs = Catalog::find()->where([
            'is_active' => 1
        ])->limit(3)->all();

        $model = Catalog::find()->where([
            'is_active' => 1
        ])->limit(1)->one();

        return $this->render('index', [
            'teachers' => $teachers,
            'banners' => $banners,
            'catalogs' => $catalogs,
            'model' => $model
        ]);
    }

}
