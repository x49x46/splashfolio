<?php

namespace app\models;

use app\components\BaseARecord;
use app\components\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $name
 * @property string $img
 * @property string $text
 * @property int $price
 * @property int $is_active
 * @property int $lang_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Catalog extends BaseARecord
{

    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'img'
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text', 'price', 'lang_id'], 'required'],
            [['price', 'is_active', 'lang_id'], 'integer'],
            [['name', 'text'], 'string'],
            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on' => 'create'],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img' => 'Картинка',
            'text' => 'Text',
            'price' => 'Цена',
            'is_active' => 'Is Active',
            'lang_id' => 'Lang ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }
}
