<?php

namespace app\models\promo;

use app\models\Image;
use Yii;
use app\models\User;
use app\components\BaseARecord;
use app\components\UploadFileBehavior;

/**
 * This is the model class for table "promo_our_photographers".
 *
 * @property int $id
 * @property int $user_id
 * @property int $image_id
 * @property int $trailer_id
 * @property string $title
 * @property string $description
 * @property int $dark_text
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class OurPhotographers extends BaseARecord
{
    public $img;

    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'img'
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_our_photographers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [
                [
                'user_id',
                'image_id',
                'trailer_id',
                'dark_text',
                'created_at',
                'updated_at',
                'author_id',
                'updater_id'
                ], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'user_id' => Yii::t('site', 'User ID'),
            'image_id' => Yii::t('site', 'Image ID'),
            'trailer_id' => Yii::t('site', 'Trailer ID'),
            'title' => Yii::t('site', 'Title'),
            'description' => Yii::t('site', 'Description'),
            'dark_text' => Yii::t('site', 'Dark text'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
        ];
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName()
    {
        return $this->user ? $this->user->username : '';
    }
}
