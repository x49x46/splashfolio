<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 02.06.2019
 * Time: 21:43
 */

namespace app\models\search;

use yii\base\Model;
use app\models\Message;
use yii\data\ActiveDataProvider;

class TranslateSearch extends Message
{
    public $message;

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'/*, 'created_at', 'updated_at', 'author_id', 'updater_id'*/], 'integer'],
            [['translation', 'language'], 'safe'],
            [['translation', 'language'], 'required']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'translation', $this->translation])
            ->andFilterWhere(['like', 'sourceMessage.message', $this->message]);

        return $dataProvider;
    }
}