<?php
/**
 * Created by PhpStorm.
 * User: IF
 * Date: 14.07.2019
 * Time: 22:27
 */

namespace app\models\search;

use yii\base\Model;
use app\models\Trailer;
use yii\data\ActiveDataProvider;

class TrailerSearch extends Trailer
{
    public $title = '';

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tutorial_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['url'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trailer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tutorial_id' => $this->tutorial_id,
            'url' => $this->url,
            'file' => $this->file,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['like', 'tutorial.title', $this->title]);

        return $dataProvider;
    }
}