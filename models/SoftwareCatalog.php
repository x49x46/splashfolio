<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "software_catalog".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $version
 * @property string $redaction
 *
 * @property Software[] $softwares
 */
class SoftwareCatalog extends \app\components\BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'software_catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name', 'description', 'version', 'redaction'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'name' => Yii::t('site', 'Name'),
            'description' => Yii::t('site', 'Description'),
            'version' => Yii::t('site', 'Version'),
            'redaction' => Yii::t('site', 'Redaction'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoftwares()
    {
        return $this->hasMany(Software::className(), ['soft_id' => 'id']);
    }
}
