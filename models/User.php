<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\IdentityInterface;
use app\components\BaseARecord;
use yii\helpers\ArrayHelper;
use app\components\UploadFileBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $avatar
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends BaseARecord implements IdentityInterface
{

    /**
     * @const string
     */
    const ROLE_GUEST = 'guest';

    /**
     * @const string
     */
    const ROLE_ADMIN = 'admin';


    /**
     * Старый пароль
     * @var string
     */
    public $password_old;

    /**
     * Повтор пароля
     * @var sting
     */
    public $password_repeat;

    /**
     * Запомнить меня
     * @var bool
     */
    public $rememberMe = false;

    /**
     * @var bool
     */
    public $is_confirm;

    /**
     * Роль
     * @var array имя роли
     */
    public $roles = [];

    /**
     * @var string
     */
    public $role;


    /**
     * Время жизни куки 1 день
     * @var int
     */
    public static $expire = 86400;


    /**
     * Список возможных ролей
     * @var array
     */
    public static $ROLES_ALL = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_GUEST => 'Гость'
    ];

    /**
     * @var object Экземпляр объекта User
     */
    private $_user;

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'avatar'
                ]
            ],
        ]);
    }

    /**
     * Правила валидации
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), [
            [['avatar'], 'safe'],
            [['username', 'password'], 'string', 'max' => 255, 'min' => 3, 'on' => 'login'],
            [['username'], 'email', 'on' => 'login'],
            [['username', 'password'], 'required', 'on' => 'login'],
            ['password', 'validatePassword', 'on' => 'login'],
            [['rememberMe'], 'safe', 'on' => 'login'],


            [['username', 'password'], 'string', 'max' => 255, 'min' => 3, 'on' => ['registration', 'create']],
            [['username', 'password', 'password_repeat'], 'required', 'on' => ['registration', 'create']],
            [['password'], 'compare', 'on' => ['registration', 'create']],
            [['username'], 'email', 'on' => ['registration', 'create']],
            [['username'], 'unique', 'on' => ['registration', 'create']],
            [['username'], 'unique', 'on' => ['registration', 'create']],
            [['roles'], 'required', 'on' => ['create']],

            [
                ['is_confirm'], 'integer', 'on' => ['registration']
            ],
            [
                ['role'], 'safe', 'on' => ['registration']
            ],
            [
                ['role'], 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_GUEST], 'on' => ['registration']
            ],


            [['username'], 'string', 'max' => 255, 'min' => 3, 'on' => 'update'],
            [['username'], 'unique', 'on' => 'update'],
            [['username'], 'required', 'on' => 'update'],
            [['roles'], 'safe', 'on' => 'update'],


            [['password', 'password_old'], 'required', 'on' => 'change_password'],
            ['password_old', 'validatePassword', 'on' => 'change_password'],
            ['password', 'validatePasswordOld', 'on' => 'change_password'],
            ['password', 'compare', 'on' => 'change_password'],
            ['password_repeat', 'safe', 'on' => 'change_password'],

            [['username', 'roles'], 'required', 'on' => ['update']],
            [['username'], 'unique', 'on' => ['update']],
            [['username'], 'email', 'on' => ['update']],

            [['id', 'username'], 'safe', 'on' => 'search'],
        ]);
    }

    /**
     * Имя аттрибутов модели
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge_recursive(parent::attributeLabels(), [
            'avatar' => Yii::t('app', 'Avatar'),
            'username' => Yii::t('app', 'E-mail'),
            'password' => Yii::t('app', 'Password'),
            'auth_key' => Yii::t('app', 'Key'),
            'access_token' => Yii::t('app', 'Токен'),
            'password_old' => Yii::t('app', 'Old password'),
            'password_repeat' => Yii::t('app', 'Повтор пароля'),
            'is_confirm' => Yii::t('app', 'Согласие с Политикой конфиденциальности и Условиями использования сервиса'),
            'roles' => Yii::t('app', 'Роли'),
        ]);
    }

    /**
     * Залогиневание
     * @return bool
     */
    public function login()
    {
        if($this->validate()) {
            $user = $this->getUser();
            return Yii::$app->user->login($user, $this->rememberMe ? 36000 : 3600);
        }
        return false;
    }

    /**
     * Получить пользователя по логину
     * @return User
     */
    public function getUser()
    {
        if(!$this->_user instanceof User) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }

    /**
     * Список пользователей
     * @return array
     */
    public static function getUsers()
    {
        return ArrayHelper::map(
            User::find()->asArray()->all(),
            'id', 'username'
        );
    }

    /**
     * Возвращает модель пользователя по id
     * @param $id int
     * @return static object
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Валидирование пароля
     * @param $attribute string Имя аттрибута
     */
    public function validatePassword($attribute)
    {
        $user = $this->getUser();
        if (!$user || !Yii::$app->getSecurity()->validatePassword($this->{$attribute},$user->password)) {
            if($attribute == 'password_old') {
                $this->addError('password_old', Yii::t('app', 'Старый пароль должен совпадать'));
            } else {
                $this->addError('password', Yii::t('app', 'Неверное имя пользователя или пароль'));
            }
        }
    }

    /**
     * Валидирование старого пароля
     * @param $attribute string Имя аттрибута
     */
    public function validatePasswordOld($attribute)
    {
        $user = $this->getUser();
        if (!$user || Yii::$app->getSecurity()->validatePassword($this->{$attribute}, $user->password)) {
            $this->addError('password', Yii::t('app', 'Новый пароль не может быть равен старому'));
        }
    }

    /**
     * Возвращает модель пользователя по access_token
     * @param $token
     * @param null $type
     * @return static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Поиск по логину пользователя
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Установка ролей
     * @param $ids array массив ролей
     */
    public function setRoles($ids) {
        $this->roles = $ids;
    }

    /**
     * Список ролей
     * @return array роли пользователя
     */
    public function getRoles()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->getId());
        $result = [];
        foreach($roles as $role) {
            if(array_key_exists($role->name, self::$ROLES_ALL)) {
                $result[] = $role->name;
            }
        }
        return $result;
    }

    /**
     * Получить id пользователя
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->username;
    }

    /**
     * Получить auth_key для пользователя
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Функция валидности поля auth_key
     * @param string $authKey
     * @return boolean если ключ валидный для пользователя
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Событие до сохранения модели
     * @return bool|void
     */
    public function beforeSave($insert)
    {
        if($this->getScenario() == 'change_password') {
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * Событие после сохранения модели
     * @return bool|void
     */
    public function afterSave($insert, $changedAttributes)
    {
        if($this->getScenario() == 'update' || $this->getScenario() == 'create') {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->getId());
            foreach($this->roles as $role) {
                $auth->assign(
                    $auth->getRole($role),
                    $this->getId()
                );
            }
        } else {
            if($this->getScenario() != 'change_password') {
                $auth = Yii::$app->authManager;
                $auth->assign(
                    $auth->getRole(self::ROLE_GUEST),
                    $this->getId()
                );
            }
        }


        if(!empty($this->role)) {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->getId());
            $auth->assign(
                $auth->getRole($this->role),
                $this->getId()
            );
        }

        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Событие до вставки модели
     * @param $event
     */
    public function beforeInsert($event)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
        $this->access_token = Yii::$app->getSecurity()->generateRandomString();
    }


    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->getId());
        return parent::beforeDelete();
    }

    /**
     * Поиск для грида
     * @param $params array
     * @return object ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => self::find(),
            'pagination' => [
                'pageSize' => BaseARecord::PAGE_SIZE
            ],
        ]);
        $this->setScenario('search');
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $dataProvider->setModels($query->all());

        return $dataProvider;
    }
}
