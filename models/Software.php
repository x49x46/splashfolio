<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "software".
 *
 * @property int $id
 * @property int $user_id
 * @property int $soft_id
 * @property int $tutorial_id
 *
 * @property SoftwareCatalog $soft
 * @property User $user
 */
class Software extends \app\components\BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'software';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'soft_id'], 'required'],
            [['user_id', 'soft_id', 'tutorial_id'], 'integer'],
            [['soft_id'], 'exist', 'skipOnError' => true, 'targetClass' => SoftwareCatalog::className(), 'targetAttribute' => ['soft_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'user_id' => Yii::t('site', 'User ID'),
            'soft_id' => Yii::t('site', 'Soft ID'),
            'tutorial_id' => Yii::t('site', 'Tutorial ID')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoft()
    {
        return $this->hasOne(SoftwareCatalog::class, ['id' => 'soft_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
