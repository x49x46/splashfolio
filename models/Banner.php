<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;
use app\components\UploadFileBehavior;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $img
 * @property int $lang_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 *
 * @property Lang $lang
 */
class Banner extends BaseARecord
{

    public function behaviors()
    {
        return array_merge_recursive(parent::behaviors(), [
            'uploadFileBehavior' => [
                'class' => UploadFileBehavior::className(),
                'attributes' => [
                    'img'
                ]
            ],
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text', 'lang_id', 'sort'], 'required'],
            [['text'], 'string'],
            [['lang_id'], 'integer'],
            [['is_active', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],

            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on' => 'create'],
            [['img'], 'file', 'extensions' => 'png, jpg, jpeg', 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Тект',
            'img' => 'Картинка',
            'lang_id' => 'Язык',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
            'is_active' => 'Активность',
            'sort' => 'Сортировка'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
}
