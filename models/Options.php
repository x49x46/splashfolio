<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo_our_photographers_misc".
 *
 * @property string $key
 * @property string $value
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * Ищет по ключу
     *
     * @param $key string
     * @return Options|null
     */
    static public function findByKey($key)
    {
        return self::findOne(['key' => $key]);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 50],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('site', 'Key'),
            'value' => Yii::t('site', 'Value'),
        ];
    }
}
