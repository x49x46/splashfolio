<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipment_catalog".
 *
 * @property int $id
 * @property string $name
 * @property string $brand
 * @property string $model
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 *
 * @property Equipment[] $equipments
 */
class EquipmentCatalog extends \app\components\BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment_catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brand', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['name', 'brand', 'model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'name' => Yii::t('site', 'Name'),
            'brand' => Yii::t('site', 'Brand'),
            'model' => Yii::t('site', 'Model'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipments()
    {
        return $this->hasMany(Equipment::className(), ['equipment_id' => 'id']);
    }
}
