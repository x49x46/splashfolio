<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property string $title
 * @property string $video
 * @property string $url
 * @property int $tutorial_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Video extends \app\components\BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tutorial_id', 'created_at', 'updated_at'], 'required'],
            [['tutorial_id', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['title', 'video', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'title' => Yii::t('site', 'Title'),
            'video' => Yii::t('site', 'Video'),
            'url' => Yii::t('site', 'Url'),
            'tutorial_id' => Yii::t('site', 'Tutorial ID'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'author_id' => Yii::t('site', 'Author ID'),
            'updater_id' => Yii::t('site', 'Updater ID'),
        ];
    }
}
